<?php

namespace Drupal\news\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\node\Entity\Node;

/**
 * Class NewsController.
 *
 * @package Drupal\news\Controller.
 */
class NewsController extends ControllerBase {

  /**
   * Return news list.
   */
  public function newsList() {

    $header = array(
      array('data' => $this->t('Title'), 'field' => 'title', 'sort' => 'asc'),
      array('data' => $this->t('Date'), 'field' => 'created'),
      array('data' => $this->t('Status')),
      array('data' => $this->t('Sub Title')),
      array('data' => $this->t('Description')),
      array('data' => $this->t('Action')),
    );


    $db = \Drupal::database();
    $query = $db->select('node_field_data', 'n');
    $query->condition('type', 'news');
    $query->fields('n', ['nid', 'title', 'created', 'status']);
    $table_sort = $query->extend('Drupal\Core\Database\Query\TableSortExtender')
      ->orderByHeader($header);
    $pager = $table_sort->extend('Drupal\Core\Database\Query\PagerSelectExtender');
    $result = $pager->execute();

    // Populate the rows.
    $rows = array();
    foreach ($result as $row) {

      $node = Node::load($row->nid);
      $sub = $node->get('field_sub_title')->value;
      $desc = $node->get('field_description')->value;

      $path = '/admin/config/content/news/edit/' . $row->nid;
      $edit = Url::fromUri('internal:' . $path);
      $edit_link = Link::fromTextAndUrl(t('Edit'), $edit)->toString();
      $path = '/admin/config/content/news/delete/' . $row->nid;

      $del = Url::fromUri('internal:' . $path);
      $del_link = Link::fromTextAndUrl(t('Delete'), $del)->toString();
      $mainLink = $this->t('@edit | @delete', array(
        '@edit' => $edit_link,
        '@delete' => $del_link,
      ));

      $row->status == '1' ? $status = 'Published' : $status = 'Not published';

      $rows[] = array(
        array('data' => $row->title),
        array('data' => date('Y-m-d', $row->created)),
        array('data' => $status),
        array('data' => $sub),
        array('data' => $desc),
        array('data' => $mainLink),
      );
    }

    // The table description.
    $build = array(
      '#markup' => $this->t('News List'),
    );

    // Generate the table.
    $build['config_table'] = array(
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
    );

    // Finally add the pager.
    $build['pager'] = array(
      '#type' => 'pager',
    );

    return $build;

  }

  /**
   * Delete content by id.
   */
  public function delete($news_id) {

    $news_deleted = \Drupal::database()->delete('node_field_data')
      ->condition('nid', $news_id)
      ->execute();

    drupal_set_message($this->t('News deleted!'));
    return new \Symfony\Component\HttpFoundation\RedirectResponse(\Drupal::url('news_list'));
  }

}
