<?php

namespace Drupal\news\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;

/**
 * Class NewsForm
 * @package Drupal\news\Form
 */
class NewsForm extends FormBase {

  /**
   * Add form id.
   */
  public function getFormId() {
    return 'news_add_form';
  }

  /**
   * Save form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $title = $form_state->getValue('title');
    $status = $form_state->getValue('status');
    $sub_title = $form_state->getValue('sub_title');
    $description = $form_state->getValue('description');
    $val = $form_state->getValue('id');

    /*Edit News*/
    if (isset($val)) {
      $id = $form_state->getValue('id');
      $node = Node::load($id);
      $node->set('title', $title);
      $node->set('status', $status);
      $node->set('sub_title', $sub_title);
      $node->set('description', $description);
      $node->enforceIsNew();
      $node->save();
      drupal_set_message(t('News saved!'));
    }
    /*Add new data.*/
    else {
      $node = Node::create([
        'type' => 'news',
        'title' => $title,
        'status' => $status,
        'field_sub_title' => $sub_title,
        'field_description' => $description,
      ]);
      $node->save();
      drupal_set_message(t('News added!'));
    }
    /*Redirect to News list page*/
    $form_state->setRedirect('news_list');
    return;
  }

  /**
   * Build form to add and edit.
   */
  public function buildForm(array $form, FormStateInterface $form_state, $news_id = NULL) {

    if ($news_id != NULL) {
      $news = $this->getEditValue($news_id);
    }
    $form['title'] = array(
      '#type' => 'textfield',
      '#description' => $this->t('Insert News shortcut name'),
      '#title' => $this->t('News name'),
      '#default_value' => $news ? $news['title'] : '',
      '#required' => TRUE,
    );
    $form['status'] = array(
      '#title' => $this->t('News status'),
      '#description' => $this->t('Chose news status'),
      '#type' => 'select',
      '#options' => array(
        0 => $this->t('Not published'),
        1 => $this->t('Published'),
      ),
      '#default_value' => $news ? $news['status'] : '',
      '#required' => TRUE,
    );
    $form['sub_title'] = array(
      '#title' => $this->t('News sub title'),
      '#description' => $this->t('Insert News sub title'),
      '#default_value' => $news ? $news['sub_title'] : '',
      '#type' => 'textfield',
    );

    $form['description'] = array(
      '#title' => $this->t('News description'),
      '#description' => $this->t('Insert News description'),
      '#default_value' => $news ? $news['description'] : '',
      '#type' => 'textarea',
    );
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $news ? $this->t('Update News') : $this->t('Add News'),
      '#button_type' => 'primary',
    );
    if ($news) {
      $form['id'] = array(
        '#type' => 'value',
        '#value' => $news['nid'],
      );
    }
    return $form;
  }

  /**
   * Default value when we edit form.
   */
  public function getEditValue($id) {

    $news = [];
    if (isset($id)) {
      $node = Node::load($id);
      $news['title'] = $node->get('title')->value;
      $news['status'] = $node->get('status')->value;
      $news['sub_title'] = $node->get('field_sub_title')->value;
      $news['description'] = $node->get('field_description')->value;

    }
    return $news;
  }

}
